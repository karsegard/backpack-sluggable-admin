<?php

namespace KDA\Sluggable\Admin;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasTranslations;
    use \KDA\Laravel\Traits\HasDynamicSidebar;

    protected $sidebars = [
        [
            'route'=>'sluggable',
            'label'=> 'Liens',
            'icon'=>'la-link'
        ]
    ];
    protected $packageName='kda-slug-admin';
    protected $viewNamespace = 'kda-slug-admin';
    protected $publishViewsTo = 'vendor/kda/backpack/slug';
    
    protected $routes = [
        'backpack/slug.php'
    ];
    protected $configs = [
        'kda/slugadmin.php'=> 'kda.slugadmin'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
