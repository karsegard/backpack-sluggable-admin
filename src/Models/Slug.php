<?php

namespace KDA\Sluggable\Admin\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Slug extends \KDA\Sluggable\Models\Slug
{
    use CrudTrait;
    
}
