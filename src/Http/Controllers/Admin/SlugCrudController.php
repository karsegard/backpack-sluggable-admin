<?php

namespace KDA\Sluggable\Admin\Http\Controllers\Admin;

use KDA\Sluggable\Admin\Http\Requests\SlugRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NavigationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SlugCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\KDA\Sluggable\Admin\Models\Slug::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sluggable');
        CRUD::setEntityNameStrings('lien', 'liens');
        $this->loadPermissions('slug');
    }

    public function collectionOptions(Request $request)
    {
        $term = $request->input('term');
        $options = \KDA\Sluggable\Models\SlugCollection::where('name', 'like', '%' . $term . '%')->get()->pluck('name', 'id');
        return $options;
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', config('kda.navadmin.max_depth', 2));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        kda_help("Gérez ou changez les liens. Les liens sont générés automatiquement lorsque vous entrez du contenu sur votre site internet. Ils servent, entre autre, pour le référencement ainsi que pour garder le contrôle");
        // select2_ajax filter
        $this->crud->addFilter(
            [
                'name'        => 'collection_id',
                'type'        => 'select2_ajax',
                'label'       => 'Collection',
                'placeholder' => 'Pick a collection'
            ],
            url('admin/sluggable/ajax-collections-options'),
            function ($value) {
                $this->crud->addClause('where', 'collection_id', $value);
            }
        );
        CRUD::addColumn([
            'name' => 'collection',
            'attribute' => 'name',
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->leftJoin('slug_collections', 'slug_collections.id', '=', 'slugs.collection_id')
                    ->orderBy('slug_collections.slug', $columnDirection)->select('slugs.*');
            }
        ]);
        CRUD::addColumn([
            'name' => 'slug',
        ]);
      /*  CRUD::addColumn([
            "name" => 'page',
            'type' => 'closure',
            'limit' => 20,
            'label' => 'Url',
            'function' => function ($entry) {
                return "<a title=\"click to copy to clipboard\" href=\"javascript:navigator.clipboard.writeText('/" . $entry->getFullPath() . "')\">" . substr($entry->getFullPath(),0,20) . "...</a>";
            },
            'priority' => 20
        ]);*/
        CRUD::addColumn([
            'name'  => 'url', // The db column name
            'label' => 'URL', // Table column heading
            'type'  => 'view',
            'limit'=>20,
            'view'  => 'kda-backpack-custom-fields::columns.url', // or path to blade file
        ]);
        CRUD::addColumn([
            'name' => 'is_current',
            'label' => 'Actif',
            'type' => 'boolean'
        ]);
        CRUD::addColumn([
            'name' => 'updated_at',
            'type'  => 'datetime',
            'format' => 'D.M.Y H:m',
        ]);
        CRUD::addColumn([
            'name' => 'sluggable',
            'attribute' => 'sluggable',
            'label'=> 'Gérer',
            'wrapper'   => [
                'element' => function ($crud, $column, $entry, $related_key) {
                    return $this->retrieveSluggableElement($crud, $column, $entry, $related_key);
                },
                'href' => function ($crud, $column, $entry, $related_key) {
                    return $this->retrieveSluggableHref($crud, $column, $entry, $related_key);
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    return $this->retrieveRelatedClass($crud, $column, $entry, $related_key);
                },
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SlugRequest::class);
        CRUD::field('slug');
    }

    protected function retrieveSluggableElement($crud, $column, $entry, $related_key)
    {
       $href = $this->retrieveSluggableHref($crud,$column,$entry,$related_key);
        if (!empty($href)) {
            return 'a';
        }
        return 'span';
    }

    protected function retrieveSluggableHref($crud, $column, $entry, $related_key)
    {
        $controller = $this->retrieveRelatedCrudController($entry->sluggable);
        if ($controller) {
            return backpack_url($controller . '/' . $related_key . '/show');
        }else {
            if(method_exists($entry->sluggable,'sluggableAdminUrl')){
                return $entry->sluggable->sluggableAdminUrl();
            }
        }
    }

    protected function retrieveRelatedCrudController($item)
    {
        $class = get_class($item);
        return match ($class) {
            default => NULL
        };
    }


    protected function retrieveRelatedClass($crud, $column, $entry, $related_key)
    {

        $class = get_class($entry->sluggable);
        return match ($class) {
            default => ''
        };
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
