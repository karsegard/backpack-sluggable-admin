<?php

return [
    'setup_routes'=>true,
    'max_depth_header'=>2,
    'max_depth_footer'=>3,
    'max_depth'=>3,
    'site_route_filter_prefix'=>'site_',
    'tab_advanced_links'=>'kda-nav-admin::nav.tab_advanced_links',
    'tab_display'=>'kda-nav-admin::nav.tab_display',
    'tab_menu'=>'kda-nav-admin::nav.tab_menu'
    
];