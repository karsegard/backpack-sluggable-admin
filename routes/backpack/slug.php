<?php
Route::group([
    'namespace'  => 'KDA\Sluggable\Admin\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    if (config('kda.slugadmin.setup_routes', true)) {
        Route::crud('sluggable', 'SlugCrudController');
    }
    Route::get('sluggable/ajax-collections-options', 'SlugCrudController@collectionOptions');

});
